#!/bin/sh

sudo umount /pmem0
sudo dmsetup remove_all
for i in 0 1 2 3; do sudo mkfs.xfs -f -d su=2m,sw=1 -m reflink=0 -f /dev/pmem$i; done
for i in  0 1 2 3; do sudo mount -o rw,noatime,nodiratime,dax /dev/pmem$i /pmem$i; done
