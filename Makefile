CC=clang  #If you use GCC, add -fno-strict-aliasing to the CFLAGS because the Google BTree does weird stuff
CFLAGS=-Wall -O0 -ggdb3
#CFLAGS=-O2 -ggdb3 -Wall
#CFLAGS=-O2 -ggdb3 -Wall  -fno-omit-frame-pointer

CXX=clang++
CXXFLAGS= ${CFLAGS} -std=c++11

LDLIBS=-lm -lpthread -lstdc++ -lpmem -lnuma
LDFLAGS=-Wl,-rpath,/usr/local/lib

MICROBENCH_OBJ=microbench.o

.PHONY: all clean

all: makefile.dep microbench

makefile.dep: *.[Cch]
	for i in *.[Cc]; do ${CC} -MM "$${i}" ${CFLAGS}; done > $@

-include makefile.dep

microbench: $(MICROBENCH_OBJ)

clean:
	rm -f *.o microbench

