#Make sure to call stripped!
#for i in 0 1 2 3; do sudo umount /pmem$i; done
#sudo ~/test_pmem/stripped.pl /dev/pmem0 /dev/pmem1 /dev/pmem2 /dev/pmem3

#ChameleonDB
#sudo rm -rf /pmem0/*
#sudo mkdir /pmem0/ShardedDB/
#sudo taskset -c 0-19 ~/tmp/ChameleonDB_source_code/build/shard w > log_chameleon_insert_few_shards_nowalflush_nolat
#
#sudo taskset -c 0-19 ~/tmp/ChameleonDB_source_code/build/shard r > log_chameleon_read_few_shards_nowalflush_nolat
#
#Masstree
#sudo rm -rf /pmem0/*
#sudo /home/blepers/RECIPE/P-Masstree/example 100000000 20 > log_masstree3

#
#exit;

#BzTree
#sudo rm -rf /pmem0/*
#sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -i 1 -r 0 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8 > log_bztree_inserts_perf
#
sudo rm -rf /pmem0/*
sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -i 0 -r 1 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8
#
#sudo rm -rf /pmem0/*
#sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -i 0.5 -r 0.5 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8 > log_bztree_ycsba
#
#sudo rm -rf /pmem0/*
#sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -i 0.05 -r 0.95 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8 > log_bztree_ycsbb
#
#sudo rm -rf /pmem0/*
#sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -s 0.95 -i 0.05 -r 0 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8 > log_bztree_ycsbe

#sudo rm -rf /pmem0/*
#sudo  numactl -C 0-19 ~/pibench/build/src/PiBench ~/bztree/build/libbztree_pibench_wrapper.so -n 100000000 -p 2000000 -s 1 -i 0 -r 0 --pool_path=/pmem0/tmp_pool --pool_size=42949672960 --pcm 0  -t 20 -k 8 -v 8 > log_bztree_scans2

#
#pmemkv csmap (requires compiling with cmake -DENGINE_CSMAP=ON -DCXX_STANDARD=14)
#sudo rm -rf /pmem0/*;
#sudo taskset -c 0-19 env PMEM_IS_PMEM_FORCE=1 ~/pmemkv-tools/pmemkv_bench --engine=csmap --db=/pmem0/db --db_size_in_gb=100 --threads=20 --num=100000000 --key_size=8 --value_size=8 --benchmarks=fillrandom,readseq,readrandom > log_pmemkv_inserts_reads2

#sudo rm -rf /pmem0/*
#sudo numactl -C 0-19 env PMEM_IS_PMEM_FORCE=1 ~/pmemkv-tools/pmemkv_bench --engine=csmap --db=/pmem0/db --db_size_in_gb=100 --threads=20 --num=10000000 --readwritepercent=50 --key_size=8 --value_size=8 > log_pmemkv_YCSB_A_8B_100M
#
#sudo rm -rf /pmem0/*
#sudo numactl -C 0-19 env PMEM_IS_PMEM_FORCE=1 ~/pmemkv-tools/pmemkv_bench --engine=csmap --db=/pmem0/db --db_size_in_gb=100 --threads=20 --num=10000000 --readwritepercent=90 --key_size=8 --value_size=8 > log_pmemkv_YCSB_B_8B_10OM
#
#sudo rm -rf /pmem0/*
#sudo numactl -C 0-19 env PMEM_IS_PMEM_FORCE=1 ~/pmemkv-tools/pmemkv_bench --engine=csmap --db=/pmem0/db --db_size_in_gb=100 --threads=20 --num=10000000 --readwritepercent=100 --key_size=8 --value_size=8 > log_pmemkv_YCSB_C_8B_10OM
#

#NoveLSM
#sudo rm -rf /pmem0/*
#~/lsm_nvm/scripts/run_dbbench.sh

#Flatstore
#sudo rm -rf /pmem0/*
#sudo rm -rf /pmem[0123]/*
#sudo ~/test_pmem/restore.sh
#
#sudo rm -rf /pmem[0123]/*
#sudo taskset -c 0-19 /home/blepers/RECIPE/P-M4/example 100000000 20 0 0 16 0 > log_flatstore_no_gc_pmempersist_taskset2
#
#sudo rm -rf /pmem[0123]/*
#sudo taskset -c 0-19 /home/blepers/RECIPE/P-M4/example 100000000 20 0 0 16 1 > log_flatstore_1_gc_pmempersist_taskset
#
#sudo rm -rf /pmem[0123]/*
#sudo taskset -c 0-19 /home/blepers/RECIPE/P-M4/example 100000000 20 0 0 16 8 > log_flatstore_8_gc_pmempersist_taskset
